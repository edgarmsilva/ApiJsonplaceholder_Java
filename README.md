## Api jsonplaceholder Test Automation 
Designed by Edgar Silva

April, 2021

This Automation was designed from scratch for test the provided APÌ in  https://jsonplaceholder.typicode.com/

###Requirements:
- Java 11+
- Maven

###The scope of this automation scope is: 
- users search validations;
- Posts search validations;
- Posts/Comments search validations;

### Execution:

To run all tests from command console:

```
mvn test
```
